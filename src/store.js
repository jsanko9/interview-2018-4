import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    dataReceived: []
  },
  mutations: {
    onDataReceived(state, payload){
      state.dataReceived = payload.data.response.venues ;
    }
  },
  getters: {
    getDataReceived: state => {
      return state.dataReceived;
    }
  }
});
